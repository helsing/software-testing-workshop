# Helsing Software testing workshop

A self-contained introductory workshop on software testing.

### Materials

- [Slides](https://helsing.gitlab.io/software-testing-workshop/)
- [Python exercises](https://gitlab.com/helsing/software-testing-workshop-python)
- [Rust exercises](https://gitlab.com/helsing/software-testing-workshop-rust)

### Contributions

Contributions are welcome. Please create a GitLab merge request and tag one of the repository owners.

### License
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).